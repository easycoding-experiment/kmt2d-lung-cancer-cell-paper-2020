---
title: "2018-02-22_SKCM_KMT2D_mut_rna"
author: "Ming Tang"
date: "February 22, 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r}
library(here)
library(TCGAbiolinks)
library(SummarizedExperiment)
library(tidyverse)

SKCM_cnt_hg38_query <- GDCquery(project = "TCGA-SKCM", 
                                data.category = "Transcriptome Profiling",
                                data.type = "Gene Expression Quantification", 
                                workflow.type = "HTSeq - Counts", 
                                legacy = FALSE)

setwd(here("data/SKCM_rna"))

GDCdownload(SKCM_cnt_hg38_query, method = "client")

SKCMRnaseqSE <- GDCprepare(SKCM_cnt_hg38_query)

## save the raw counts.
save(SKCMRnaseqSE, file = here("data/TCGA_RNAseq/SKCMRnaseqSE.rda"))
load(here("data/TCGA_RNAseq/SKCMRnaseqSE.rda"))

SKCM_maf_hg19_query <- GDCquery(project = "TCGA-SKCM", 
                           data.category = "Simple nucleotide variation", 
                           data.type = "Simple somatic mutation",
                           access = "open", 
                           legacy = TRUE)
# Check maf availables
knitr::kable(getResults(LUSC_maf_hg19_query)[,c("created_datetime","file_name")]) 

setwd(here("data/SKCM_mut"))
GDCdownload(SKCM_maf_hg19_query, method = "client")
## this is working
SKCM_maf_hg19 <- GDCprepare(SKCM_maf_hg19_query)

save(SKCM_maf_hg19, file = here("data/SKCM_mut/SKCM_maf_hg19.rda"))
load(here("data/SKCM_mut/SKCM_maf_hg19.rda"))
# go back to the top dir
setwd(here())
```


```{r}

SKCM_counts<- assay(SKCMRnaseqSE)

SKCM_maf_hg19 %>% head() %>% View()

load(SKCM_maf_hg19, file = "data/SKCM_mut/SKCM_maf_hg19.rda")

library(stringr)
library(janitor)
all_samples<- SKCM_maf_hg19 %>% pull(Tumor_Sample_Barcode) %>% str_sub(1,16) %>% unique()

SKCM_maf_hg19 %>% 
  dplyr::select(Hugo_Symbol, Tumor_Sample_Barcode, Variant_Classification, Variant_Type) %>% 
  dplyr::filter(Hugo_Symbol == "KMT2D") %>%
  tabyl(Variant_Classification) 


SKCM_KMT2D_mutants<- SKCM_maf_hg19 %>% 
  dplyr::select(Hugo_Symbol, Tumor_Sample_Barcode, Variant_Classification, Variant_Type) %>%
  dplyr::filter(Hugo_Symbol == "KMT2D") %>%
  pull(Tumor_Sample_Barcode) %>% str_sub(1,16) %>% unique()

SKCM_KMT2D_WT<- setdiff(all_samples, SKCM_KMT2D_mutants)

SKCM_KMT2D_nonsense<- SKCM_maf_hg19 %>% 
  dplyr::select(Hugo_Symbol, Tumor_Sample_Barcode, Variant_Classification, Variant_Type) %>%
  dplyr::filter(Hugo_Symbol == "KMT2D", Variant_Classification == "Nonsense_Mutation") %>%
  pull(Tumor_Sample_Barcode) %>% str_sub(1,16) %>% unique()

## no normals in the RNAseq table
colnames(SKCM_counts) %>% str_sub(15,16) %>% table()

## compare primary vs metastatic SKCM

colData(SKCMRnaseqSE) %>% as.data.frame() %>% head() %>% View()

SKCM_anno<- colData(SKCMRnaseqSE) %>% as.data.frame() %>%
  dplyr::select(sample, patient, barcode, shortLetterCode, definition, tumor_stage, subtype_MUTATIONSUBTYPES, subtype_RNASEQ.CLUSTER_CONSENHIER) %>%
  rename(mutation_subtype = subtype_MUTATIONSUBTYPES, rnaseq_subtype = subtype_RNASEQ.CLUSTER_CONSENHIER  )


## convert to TPM
#source("https://bioconductor.org/biocLite.R")
#biocLite("EnsDb.Hsapiens.v83")

#create a tx2gene.txt table
library(EnsDb.Hsapiens.v86)
edb <- EnsDb.Hsapiens.v86

genes_ensemble <- genes(edb)

gene_length<- as.data.frame(genes_ensemble) %>% dplyr::select(gene_id, gene_name, width)


gene_length_in_SKCM_mat<- left_join(data.frame(gene_id = rownames(SKCM_counts)), gene_length) %>% dplyr::filter(!is.na(width))

SKCM_mat_sub<- SKCM_counts[rownames(SKCM_counts) %in% gene_length_in_SKCM_mat$gene_id, ]
## now, just use gene_length instead of effective length. effective length = gene length - fragment_length

all.equal(rownames(SKCM_mat_sub), gene_length_in_SKCM_mat$gene_id)

countToTpm <- function(counts, effLen)
{
    rate <- log(counts + 1) - log(effLen)
    denom <- log(sum(exp(rate)))
    exp(rate - denom + log(1e6))
}


SKCM_TPM<- apply(SKCM_mat_sub, 2, countToTpm, effLen = gene_length_in_SKCM_mat$width)
## for KMT2D

KMT2D_exp<- data.frame( barcode = colnames(SKCM_TPM), TPM = SKCM_TPM["ENSG00000167548", ])

KMT2D_exp %>% left_join(SKCM_anno) %>% 
  mutate(definition = if_else(definition == "Additional Metastatic", "Metastatic", definition)) %>%
  ggplot(aes(x = definition, y = TPM)) +
  geom_boxplot(aes(col = definition)) + 
  theme_bw(base_size = 14)


KMT2D_exp %>% left_join(SKCM_anno) %>% 
  mutate(definition = if_else(definition == "Additional Metastatic", "Metastatic", definition)) %>%
  dplyr::filter(! is.na(rnaseq_subtype) & rnaseq_subtype != "-") %>%
  ggplot(aes(x = rnaseq_subtype, y = TPM)) + 
  #facet_wrap(~ definition) + 
  geom_boxplot(aes(col = rnaseq_subtype)) + 
  theme_bw(base_size = 14)

#install.packages("EnvStats")
# to use https://stackoverflow.com/questions/40102613/ggplot2-adding-sample-size-information-to-x-axis-tick-labels
library(EnvStats)
KMT2D_exp %>% left_join(SKCM_anno) %>% 
  mutate(definition = if_else(definition == "Additional Metastatic", "Metastatic", definition)) %>%
  dplyr::filter(! is.na(rnaseq_subtype) & rnaseq_subtype != "-") %>%
  ggplot(aes(x = rnaseq_subtype, y = TPM)) + 
  #facet_wrap(~ definition) + 
  stat_n_text() + 
  geom_boxplot(aes(col = rnaseq_subtype)) + 
  theme_bw(base_size = 14)

KMT2D_exp %>% left_join(SKCM_anno) %>% 
  mutate(definition = if_else(definition == "Additional Metastatic", "Metastatic", definition)) %>%
  dplyr::filter(! is.na(rnaseq_subtype) & rnaseq_subtype != "-") %>%
  ggplot(aes(x = rnaseq_subtype, y = TPM)) + 
  stat_n_text() +
  facet_wrap(~ definition) + 
  geom_boxplot(aes(col = rnaseq_subtype)) + 
  theme_bw(base_size = 14)

SKCM_anno_met<- readxl::read_xlsx("~/TCGA_Melanoma_ID_Metadata_MetastaticTumors_only.xlsx")

KMT2D_exp %>% 
  mutate(Name = str_sub(barcode, 1, 15)) %>%
  inner_join(SKCM_anno_met) %>%
  rename(rnaseq_subtype = `RNASEQ-CLUSTER_CONSENHIER`) %>%
  ggplot(aes(x = rnaseq_subtype, y = TPM)) + 
  stat_n_text() +
  geom_boxplot(aes(col = rnaseq_subtype)) + 
  theme_bw(base_size = 14)
  
  

```


### need to compare for primary and metastatic seprately.

```{r}
primary_samples<- SKCM_anno %>% dplyr::filter(definition == "Primary solid Tumor") %>%
  pull(sample)

metastatic_samples<- SKCM_anno %>% dplyr::filter(definition == "Metastatic") %>% 
  pull(sample)


primary_samples_KMT2D_WT<- intersect(primary_samples, SKCM_KMT2D_WT)
metastatic_samples_KMT2D_WT<- intersect(metastatic_samples, SKCM_KMT2D_WT)

KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% primary_samples_KMT2D_WT) %>% 
  ggplot(aes(x = TPM)) + geom_histogram(col = "white")

## top 10 and bottom 10 samples of KMT2D expression
KMT2D_WT_primary_top10<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% primary_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% head(10)

KMT2D_WT_primary_bottom10<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% primary_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% tail(10)

######################### for metastatic
KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT) %>% 
  ggplot(aes(x = TPM)) + geom_histogram(col = "white")


KMT2D_WT_metastatic_top10<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% head(10)

KMT2D_WT_metastatic_bottom10<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% tail(10)


```

### DESeq2 for differential expression


primary
```{r}
library(DESeq2)
colnames(SKCM_counts)<- colnames(SKCM_counts) %>% str_sub(1,16)

SKCM_primary_top10_bottom10<- SKCM_counts[,c(KMT2D_WT_primary_top10, KMT2D_WT_primary_bottom10)]

coldata_SKCM_primary<- data.frame(condition = rep(c("high", "low"), each = 10))

rownames(coldata_SKCM_primary)<- c(KMT2D_WT_primary_top10, KMT2D_WT_primary_bottom10)
write.table(coldata_SKCM_primary, "data/TCGA_RNAseq/metadata_SKCM_primary_KMT2D_WT_low_vs_high.txt", col.names = T, row.names = T, quote =F, sep = "\t")

all.equal(rownames(coldata_SKCM_primary), colnames(SKCM_primary_top10_bottom10))

dds_SKCM_primary <- DESeqDataSetFromMatrix(countData = SKCM_primary_top10_bottom10,
                              colData = coldata_SKCM_primary,
                              design = ~ condition)
dds_SKCM_primary 

dds_SKCM_primary$condition <- factor(dds_SKCM_primary$condition, levels = c("low","high"))
## at least 5 sample has counts >=5
dds_SKCM_primary <- dds_SKCM_primary[ rowSums(counts(dds_SKCM_primary) >= 5) >= 5, ]

vsd.fast_SKCM_primary <- vst(dds_SKCM_primary, blind=FALSE)

plotPCA(vsd.fast_SKCM_primary, intgroup=c("condition"), ntop = 1000)


## differential expression

dds_SKCM_primary<- DESeq(dds_SKCM_primary)

res_SKCM_primary <- results(dds_SKCM_primary, contrast=c("condition", "low", "high"))


res_SKCM_primary$ENSEMBL<- rownames(res_SKCM_primary)

res_anno_SKCM_primary<- as.data.frame(res_SKCM_primary) %>% left_join(gene_length, c("ENSEMBL" = "gene_id"))

res_SKCM_primary_pre_rank<- sign(res_anno_SKCM_primary$log2FoldChange) * -log10(res_anno_SKCM_primary$pvalue)

write_tsv(data.frame(Name = res_anno_SKCM_primary$gene_name, metric = res_SKCM_primary_pre_rank) %>% na.omit(), "results/TCGA_SKCM_primary_KMT2D_10low_vs_10high_pre_rank.rnk")

res_anno_SKCM_primary %>% dplyr::filter(gene_name == "KMT2D")


###### Heatmap

primary_diff_genes<- res_anno_SKCM_primary %>% dplyr::filter(abs(log2FoldChange) >=1, padj <= 0.05) %>% pull(ENSEMBL)


rlog_primary_mat<- assay(vsd.fast_SKCM_primary)[rownames(assay(vsd.fast_SKCM_primary)) %in% primary_diff_genes , ]

primary_scaled_mat<- t(scale(t(rlog_primary_mat), center =T, scale =F))

library(ComplexHeatmap)


ha_primary<- HeatmapAnnotation(df = coldata_SKCM_primary)
col_fun3<- circlize::colorRamp2(c(-3, 0, 3), c("blue", "white", "red"))
Heatmap(primary_scaled_mat, cluster_rows = T, cluster_columns = T, show_column_names = F, show_row_names = F,
        bottom_annotation = ha_primary,
        col = col_fun3,
        clustering_distance_rows = "pearson",
        clustering_distance_columns = "pearson",
        clustering_method_rows = "ward.D2",
        clustering_method_columns = "ward.D2",
        name = "scaled gene\nexpression")
```

metastatic

```{r}

SKCM_metastatic_top10_bottom10<- SKCM_counts[,c(KMT2D_WT_metastatic_top10, KMT2D_WT_metastatic_bottom10)]

coldata_SKCM_metastatic<- data.frame(condition = rep(c("high", "low"), each = 10))

rownames(coldata_SKCM_metastatic)<- c(KMT2D_WT_metastatic_top10, KMT2D_WT_metastatic_bottom10)
write.table(coldata_SKCM_metastatic, "data/TCGA_RNAseq/metadata_SKCM_metastatic_KMT2D_WT_low_vs_high.txt", col.names = T, row.names = T, quote =F, sep = "\t")

all.equal(rownames(coldata_SKCM_metastatic), colnames(SKCM_metastatic_top10_bottom10))

dds_SKCM_metastatic<- DESeqDataSetFromMatrix(countData = SKCM_metastatic_top10_bottom10,
                              colData = coldata_SKCM_metastatic,
                              design = ~ condition)

dds_SKCM_metastatic$condition <- factor(dds_SKCM_metastatic$condition, levels = c("low","high"))
## at least 5 sample has counts >=5
dds_SKCM_metastatic<- dds_SKCM_metastatic[ rowSums(counts(dds_SKCM_metastatic) >= 5) >= 5, ]

vsd.fast_SKCM_metastatic <- vst(dds_SKCM_metastatic, blind=FALSE)

plotPCA(vsd.fast_SKCM_metastatic, intgroup=c("condition"), ntop = 1000)

## differential expression

dds_SKCM_metastatic <- DESeq(dds_SKCM_metastatic)

res_SKCM_metastatic <- results(dds_SKCM_metastatic, contrast=c("condition", "low", "high"))


res_SKCM_metastatic$ENSEMBL<- rownames(res_SKCM_metastatic)

res_anno_SKCM_metastatic<- as.data.frame(res_SKCM_metastatic) %>% left_join(gene_length, c("ENSEMBL" = "gene_id"))

res_SKCM_metastatic_pre_rank<- sign(res_anno_SKCM_metastatic$log2FoldChange) * -log10(res_anno_SKCM_metastatic$pvalue)

write_tsv(data.frame(Name = res_anno_SKCM_metastatic$gene_name, metric = res_SKCM_metastatic_pre_rank) %>% na.omit(), "results/TCGA_SKCM_metastatic_KMT2D_10low_vs_10high_pre_rank.rnk")


res_anno_SKCM_metastatic %>% dplyr::filter(gene_name == "KMT2D")

###### Heatmap

metastatic_diff_genes<- res_anno_SKCM_metastatic %>% dplyr::filter(abs(log2FoldChange) >=1, padj <= 0.05) %>% pull(ENSEMBL)


rlog_metastatic_mat<- assay(vsd.fast_SKCM_metastatic)[rownames(assay(vsd.fast_SKCM_metastatic)) %in% primary_diff_genes , ]

metastatic_scaled_mat<- t(scale(t(rlog_metastatic_mat), center =T, scale =F))

library(ComplexHeatmap)


ha_metastatic<- HeatmapAnnotation(df = coldata_SKCM_metastatic)

col_fun3<- circlize::colorRamp2(c(-3, 0, 3), c("blue", "white", "red"))
Heatmap(metastatic_scaled_mat, cluster_rows = T, cluster_columns = T, show_column_names = F, show_row_names = F,
        bottom_annotation = ha_metastatic,
        col = col_fun3,
        clustering_distance_rows = "pearson",
        clustering_distance_columns = "pearson",
        clustering_method_rows = "ward.D2",
        clustering_method_columns = "ward.D2",
        name = "scaled gene\nexpression")
```


### compare SKCM KMT2D mutants and wild-type high samples

```{r}
SKCM_maf_hg19 %>% head() %>% View()

SKCM_KMT2D_mutants<- SKCM_maf_hg19 %>% dplyr::filter(Hugo_Symbol == "KMT2D", Variant_Classification != "Silent") %>%
  dplyr::select(Chromosome, Start_position, End_position, Reference_Allele, Tumor_Seq_Allele2, Protein_Change, Hugo_Symbol, Variant_Classification, Tumor_Sample_Barcode)

SKCM_KMT2D_SET_domain<- SKCM_KMT2D_mutants %>% 
  mutate(protein_aa_position = gsub("p.[A-Z]+([0-9]+)[A-Za-z*]+", "\\1", Protein_Change)) %>% 
  mutate(protein_aa_position = as.numeric(protein_aa_position)) %>%
  dplyr::filter(protein_aa_position >= 4600 | Variant_Classification %in% c("Frame_Shift_Del", "Nonsense_Mutation")) 


write_tsv(SKCM_KMT2D_SET_domain, "results/SKCM_KMT2D_SET_domain.txt", col_names = F)
```


### use Annovar to annotate 

```{bash}
perl /scratch/genomic_med/apps/annovar/annovar/table_annovar.pl SKCM_KMT2D_SET_domain.txt /scratch/genomic_med/apps/annovar/annovar/humandb/ -buildver hg19 -out SKCM_KMT2D_SET_domain -remove -protocol refGene,cytoBand,snp129,cosmic70,dbnsfp33a,clinvar_20170130  -operation g,r,f,f,f,f -nastring NA -polish

rsync -avhP railab:scratch/SKCM_MLL2/SKCM_KMT2D_SET_domain.hg19_multianno.txt results/


```

### SKCM KMT2D 12 mutants vs 12 WT high 

```{r}
cadd_anno<- read_tsv("results/SKCM_KMT2D_SET_domain.hg19_multianno.txt", col_name = T)
cadd_anno<- dplyr::select(cadd_anno, Chr, Start, End, Ref, Alt, CADD_phred)
cadd_anno$Chr<- as.character(cadd_anno$Chr)

## total 16 samples
SKCM_KMT2D_SET_affected_samples<- SKCM_KMT2D_SET_domain %>% 
  left_join(cadd_anno, by = c("Chromosome" = "Chr", "Start_position" = "Start", "End_position" = "End" )) %>%
  dplyr::filter(CADD_phred >= 20 | is.na(CADD_phred)) %>%
  pull(Tumor_Sample_Barcode) %>% unique()

SKCM_KMT2D_SET_affected_met_samples<- SKCM_KMT2D_SET_affected_samples[-c(2,5,12,15)] %>% str_sub(1,16)
## now choose 12 KMT2D WT samples with the highest expression.



KMT2D_WT_metastatic_top11<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% head(11)



```


```{r}
library(DESeq2)
SKCM_metastatic_top11_KMT2D_11_mut<- SKCM_counts[,c(KMT2D_WT_metastatic_top11, SKCM_KMT2D_SET_affected_met_samples)]

coldata_SKCM_metastatic_KMT2D_mut<- data.frame(condition = rep(c("WT", "Mutant"), each = 11))

rownames(coldata_SKCM_metastatic_KMT2D_mut)<- c(KMT2D_WT_metastatic_top11, SKCM_KMT2D_SET_affected_met_samples)
write.table(coldata_SKCM_metastatic_KMT2D_mut, "data/TCGA_RNAseq/metadata_SKCM_metastatic_KMT2D_mut_vs_high.txt", col.names = T, row.names = T, quote =F, sep = "\t")

all.equal(rownames(coldata_SKCM_metastatic_KMT2D_mut), colnames(SKCM_metastatic_top11_KMT2D_11_mut))

dds_SKCM_metastatic_KMT2D_mut<- DESeqDataSetFromMatrix(countData = SKCM_metastatic_top11_KMT2D_11_mut,
                              colData = coldata_SKCM_metastatic_KMT2D_mut,
                              design = ~ condition)

dds_SKCM_metastatic_KMT2D_mut$condition <- factor(dds_SKCM_metastatic_KMT2D_mut$condition)

## at least 5 sample has counts >=5
dds_SKCM_metastatic_KMT2D_mut<- dds_SKCM_metastatic_KMT2D_mut[ rowSums(counts(dds_SKCM_metastatic_KMT2D_mut) >= 5) >= 5, ]

vsd.fast_SKCM_metastatic_KMT2D_mut <- vst(dds_SKCM_metastatic_KMT2D_mut, blind=FALSE)

plotPCA(vsd.fast_SKCM_metastatic_KMT2D_mut, intgroup=c("condition"), ntop = 1000)

## differential expression

dds_SKCM_metastatic_KMT2D_mut <- DESeq(dds_SKCM_metastatic_KMT2D_mut)

res_SKCM_metastatic_KMT2D_mut <- results(dds_SKCM_metastatic_KMT2D_mut, contrast=c("condition", "Mutant", "WT"))


res_SKCM_metastatic_KMT2D_mut$ENSEMBL<- rownames(res_SKCM_metastatic_KMT2D_mut)

res_anno_SKCM_metastatic_KMT2D_mut<- as.data.frame(res_SKCM_metastatic_KMT2D_mut) %>% left_join(gene_length, c("ENSEMBL" = "gene_id"))

res_SKCM_metastatic_KMT2D_pre_rank<- sign(res_anno_SKCM_metastatic_KMT2D_mut$log2FoldChange) * -log10(res_anno_SKCM_metastatic_KMT2D_mut$pvalue)

write_tsv(data.frame(Name = res_anno_SKCM_metastatic_KMT2D_mut$gene_name, metric = res_SKCM_metastatic_KMT2D_pre_rank) %>% na.omit(), "results/TCGA_SKCM_metastatic_KMT2D_11mutants_vs_11high_pre_rank.rnk")


res_anno_SKCM_metastatic_KMT2D_mut %>% dplyr::filter(gene_name == "IGFBP7")
```


### two truncating mutants and two frame-shift deletion KMT2D mutants vs 4 KMT2D WT high 

```{r}

KMT2D_nonfunctional_mutants<- SKCM_KMT2D_SET_domain %>%
  dplyr::filter(Variant_Classification %in% c("Frame_Shift_Del", "Nonsense_Mutation")) %>%
  pull(Tumor_Sample_Barcode) %>%
  str_sub(1,16)


KMT2D_WT_metastatic_top4<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT) %>% 
  arrange(desc(TPM)) %>% pull(sample) %>% head(4)


library(DESeq2)
SKCM_metastatic_top4_KMT2D_4_mut<- SKCM_counts[,c(KMT2D_WT_metastatic_top4, KMT2D_nonfunctional_mutants)]

coldata_SKCM_metastatic_KMT2D_4mut<- data.frame(condition = rep(c("WT", "Mutant"), each = 4))

rownames(coldata_SKCM_metastatic_KMT2D_4mut)<- c(KMT2D_WT_metastatic_top4, KMT2D_nonfunctional_mutants)
write.table(coldata_SKCM_metastatic_KMT2D_4mut, "data/TCGA_RNAseq/metadata_SKCM_metastatic_KMT2D_4mut_vs_high.txt", col.names = T, row.names = T, quote =F, sep = "\t")

all.equal(rownames(coldata_SKCM_metastatic_KMT2D_4mut), colnames(SKCM_metastatic_top4_KMT2D_4_mut))

dds_SKCM_metastatic_KMT2D_4mut<- DESeqDataSetFromMatrix(countData = SKCM_metastatic_top4_KMT2D_4_mut,
                              colData = coldata_SKCM_metastatic_KMT2D_4mut,
                              design = ~ condition)

dds_SKCM_metastatic_KMT2D_4mut$condition <- factor(dds_SKCM_metastatic_KMT2D_4mut$condition)

## at least 5 sample has counts >=5
dds_SKCM_metastatic_KMT2D_4mut<- dds_SKCM_metastatic_KMT2D_4mut[ rowSums(counts(dds_SKCM_metastatic_KMT2D_4mut) >= 5) >= 2, ]

vsd.fast_SKCM_metastatic_KMT2D_4mut <- vst(dds_SKCM_metastatic_KMT2D_4mut, blind=FALSE)

plotPCA(vsd.fast_SKCM_metastatic_KMT2D_4mut, intgroup=c("condition"), ntop = 1000)

## differential expression

dds_SKCM_metastatic_KMT2D_4mut <- DESeq(dds_SKCM_metastatic_KMT2D_4mut)

res_SKCM_metastatic_KMT2D_4mut <- results(dds_SKCM_metastatic_KMT2D_4mut, contrast=c("condition", "Mutant", "WT"))


res_SKCM_metastatic_KMT2D_4mut$ENSEMBL<- rownames(res_SKCM_metastatic_KMT2D_4mut)

res_anno_SKCM_metastatic_KMT2D_4mut<- as.data.frame(res_SKCM_metastatic_KMT2D_4mut) %>% left_join(gene_length, c("ENSEMBL" = "gene_id"))

res_SKCM_metastatic_4KMT2D_pre_rank<- sign(res_anno_SKCM_metastatic_KMT2D_4mut$log2FoldChange) * -log10(res_anno_SKCM_metastatic_KMT2D_4mut$pvalue)

write_tsv(data.frame(Name = res_anno_SKCM_metastatic_KMT2D_4mut$gene_name, metric = res_SKCM_metastatic_4KMT2D_pre_rank) %>% na.omit(), "results/TCGA_SKCM_metastatic_KMT2D_4mutants_vs_4high_pre_rank.rnk")


res_anno_SKCM_metastatic_KMT2D_4mut %>% dplyr::filter(gene_name == "IGFBP1")
```

### pAKT levels in MLL2 mutants vs WT

```{r}

SKCM_KMT2D_SET_affected_met_samples

KMT2D_WT_metastatic_TPM40<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 16)) %>% dplyr::filter(sample %in% metastatic_samples_KMT2D_WT, TPM >= 40) 
  
SKCM_RPPA<- read_csv("~/Downloads/TCGA-SKCM-L4.csv") 
SKCM_RPPA<- SKCM_RPPA %>% dplyr::select(Sample_ID, contains("AKT")) %>% mutate(sample = str_sub(Sample_ID, 1, 16))

SKCM_RPPA_AKT<- SKCM_RPPA %>% mutate(KMT2D_status = case_when(
  sample %in% SKCM_KMT2D_SET_affected_met_samples ~ "mut",
  sample %in% KMT2D_WT_metastatic_top11 ~ "WT_high"
)) %>% dplyr::filter(!is.na(KMT2D_status))

table(SKCM_RPPA_AKT$KMT2D_status)
p1<- ggplot(SKCM_RPPA_AKT, aes(x = KMT2D_status, y = AKT))+
  geom_boxplot()

p2<- ggplot(SKCM_RPPA_AKT, aes(x = KMT2D_status, y = AKT_pS473)) +
  geom_boxplot()

p3<- ggplot(SKCM_RPPA_AKT, aes(x= KMT2D_status, y =AKT_pT308)) +
  geom_boxplot()

(p1 + p2 + p3) * ggtitle("TCGA SKCM RPPA")
```



### IGFBP levels in MLL2 mutants vs MLL2 WT

```{r}
SKCM_KMT2D_SET_affected_samples %>% str_sub(1,15)
SKCM_WT_top15<- KMT2D_exp %>% mutate(sample = str_sub(barcode, 1, 15)) %>% arrange(desc(TPM)) %>% pull(sample) %>%
  head(15)

colnames(SKCM_TPM)<- colnames(SKCM_TPM) %>% str_sub(1, 15)
SKCM_TPM[1:6, 1:6]

IGFBPs<- c("ENSG00000146678", "ENSG00000115457", "ENSG00000146674", "ENSG00000141753", "ENSG00000115461", "ENSG00000167779",
           "ENSG00000163453")

IGFBP_exp<- SKCM_TPM[IGFBPs, c(SKCM_WT_top15, SKCM_KMT2D_SET_affected_samples %>% str_sub(1,15))]

IGFBP_exp_df<- IGFBP_exp %>% as.data.frame() %>% mutate(gene = paste0("IGFBP", 1:7)) %>% gather(sample, TPM, -gene) %>%
  mutate(KMT2D_status = case_when(
    sample %in% SKCM_WT_top15 ~ "WT",
    TRUE ~ "KMT2D_mut"
  ))
  

ggplot(IGFBP_exp_df, aes(x = KMT2D_status, y = log2(TPM)))+ geom_boxplot(aes(col = KMT2D_status), outlier.colour = NA) +
  geom_jitter(width = 0.2) +
         facet_wrap(~gene,scales ="free") +
  theme_minimal(base_size = 14)

t.test(log2(TPM) ~ KMT2D_status, data = IGFBP_exp_df %>% filter(gene == "IGFBP2"), alternative = "less")
```


