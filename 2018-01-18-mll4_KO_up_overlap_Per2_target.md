```r

library(ChIPseeker)
library(rtracklayer)

bed<- import("Per2_peaks.bed", format= "BED")

library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene

## see https://github.com/GuangchuangYu/ChIPseeker/issues/55
#
Per2_anno <- annotatePeak(bed, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "TSS",
                          addFlankGeneInfo = TRUE,  flankDistance = 2000)


Per2_anno<-Per2_anno %>% 
  as.data.frame()%>% 
  dplyr::select(seqnames,start, end, geneId, distanceToTSS, SYMBOL, flank_geneIds, flank_gene_distances) %>%
  mutate(flank_geneIds =strsplit(as.character(flank_geneIds), ";")) %>% 
  mutate(flank_gene_distances =strsplit(as.character(flank_gene_distances), ";")) %>%
  unnest(flank_geneIds, flank_gene_distances) 


## get the gene symbol for the flank genes.
Per2_flank_gene_symbols<- AnnotationDbi::select(org.Mm.eg.db, keys=Per2_anno$flank_geneIds, 
                                    columns="SYMBOL", keytype="ENTREZID") %>% dplyr::rename(flank_symbol= SYMBOL)

all.equal(Per2_anno$flank_geneIds, Per2_flank_gene_symbols$ENTREZID)

Per2_anno<- cbind(Per2_anno, Per2_flank_gene_symbols) %>% dplyr::select(-ENTREZID)


write.table(Per2_anno,  "Per2_Peaks_anno.tsv", quote =F, sep = "\t", row.names =F, col.names = T)
```

### Per2 ChIPseq peak target genes.

```bash

less -S Per2_Peaks_anno.tsv | cut -f6,9 | tr "\t" "\n" | sort | uniq > Per2_Peak_target_genes.txt

less -S mll4_vs_wt_htseq_DESeq2.tsv | csvtk filter  -t -f  "padj<=0.05" | csvtk filter -t -f "log2FoldChange>=0.5" > mll4_ko_vs_wt_up_FDR0.05_genes.txt

less -S mll4_ko_vs_wt_up_FDR0.05_genes.txt | sed '1d' | cut -f7 > mll4_ko_vs_wt_up_gene_symbol.txt

## what are the genes are up-regulated in mll4 ko and also are targets of Per2. (Per2 in Mll4 Ko sample decreases, it is a transcriptional co-repressor)

comm -12 <(sort Per2_Peak_target_genes.txt)  <(sort mll4_ko_vs_wt_up_gene_symbol.txt)
Aldh7a1
Col18a1
Cystm1
Eif3l
Eif4b
Foxq1
Grhpr
Hsp90ab1
Oit1
S100a14
Scel
Sdc1
Slc38a2
Syngr1
Syt8
Tmem45b
Traf4

```





