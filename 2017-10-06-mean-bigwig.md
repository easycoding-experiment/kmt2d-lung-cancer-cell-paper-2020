merge the resequenced ChIP-seq, run through pyflow-ChIPseq pipeline.
get the bigwigs, make a mean bigwig file for each mark using wiggletools

```bash
#! /bin/bash

set -euo pipefail

type=$1
mark=$2

wiggletools mean ${type}_*_${mark}.bw | wigToBigWig stdin mm9_genome.info ${type}_${mark}_mean.bw


```

```bash
for mark in {A,B,C,D,E,F}; do echo ./make_mean_bw.sh RAS $mark > RAS_$mark.commands.txt ; done
for mark in {A,B,C,D,E,F}; do echo ./make_mean_bw.sh MLL $mark > MLL_$mark.commands.txt ; done

find *txt | parallel 'makebsub -a {} -j{/.} -q short -t 1:00 -c 2 > {/.}.bsub
```

