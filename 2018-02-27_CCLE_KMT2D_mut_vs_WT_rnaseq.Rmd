---
title: "2018-02-27_CCLE_KMT2D_mut_vs_WT"
author: "Ming Tang"
date: "February 27, 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{bash}
cd /home/mtang1/projects/CCLE_project/data/CCLE_public_data/rnaseq

for LUAD: 
NCIH1568_LUAD_MLL4 mutant
DV90_LUAD_ MLL4 mutant
CORL105_LUAD_ MLL4 mutant
NCI H1473_LUAD_MLL4 WT
NCI H358_LUAD_MLL4 WT
NCI H23_LUAD_MLL4 WT
NCI H1792_LUAD_MLL4 WT
NCI H1975_LUAD_MLL4 WT
NCI H322_LUAD_MLL4 WT
NCI H2122_LUAD_MLL4 WT

cat list.txt

Description
NCIH1568_LUNG
DV90_LUNG
CORL105_LUNG
NCIH1473_LUNG
NCIH358_LUNG
NCIH23_LUNG
NCIH1792_LUNG
NCIH1975_LUNG
NCIH322_LUNG
NCIH2122_LUNG


# "NCIH1473_LUNG" not in the file, remove it in the list.txt and do
cat CCLE_RNAseq_081117.reads.gct | awk 'NR > 2' | csvtk cut -t -f $(paste -s -d , list.txt) - > Lung_ADC_reads.txt
```



```{r}
library(DESeq2)

cts<- read_tsv("~/projects/CCLE_project/data/CCLE_public_data/rnaseq/Lung_ADC_reads.txt")

cts<- as.data.frame(cts) %>% distinct(Description, .keep_all =T)

rownames(cts)<- cts$Description %>% str_replace("_LUNG", "")

cts<- as.matrix(cts[,-1])

cts<- cts[,1:6]
head(cts)

coldata_ccle<- data.frame(condition = c(rep("KMT2D_mut",3), rep("WT", 3)))
rownames(coldata_ccle)<- colnames(cts)
dds_ccle <- DESeqDataSetFromMatrix(countData = cts,
                              colData = coldata_ccle,
                              design = ~ condition)
dds_ccle

```


```{r}
dds_ccle$condition <- factor(dds_ccle$condition)
## at least 5 sample has counts >=5
dds_ccle<- dds_ccle[ rowSums(counts(dds_ccle) >= 5) >= 1, ]

vsd.fast_ccle <- vst(dds_ccle, blind=FALSE)

plotPCA(vsd.fast_ccle, intgroup=c("condition"), ntop = 1000)

dds_ccle<- DESeq(dds_ccle)

res_ccle<- results(dds_ccle, contrast=c("condition", "KMT2D_mut", "WT"))


res_ccle$gene_name<- rownames(res_ccle)

res_ccle_pre_rank<- sign(res_ccle$log2FoldChange) * -log10(res_ccle$pvalue)

write_tsv(data.frame(Name = res_ccle$gene_name, metric = res_ccle_pre_rank) %>% na.omit(), "results/CCLE_KMT2D_3mut_vs_3WT_pre_rank.rnk")

res_ccle %>% as.data.frame() %>% dplyr::filter(gene_name == "TGFB1")


ccle_norm["TGFBI",]

## only use fold change to prerank NCIH1568 vs NCIH358
ccle_norm<- assay(vsd.fast_ccle)

as.data.frame(ccle_norm) %>% rownames_to_column("gene_name") %>% inner_join(as.data.frame(res_ccle)) %>% write_tsv("results/CCLE_KMT2D_mut_vs_WT_rlog_counts.tsv")
write_tsv(data.frame(Name = rownames(ccle_norm), metric = ccle_norm[,"NCIH1568_LUNG"]/ccle_norm[, "NCIH358_LUNG"]) %>% na.omit(), "results/CCLE_KMT2D_nonsense_vs_WT_fold_change_pre_rank.rnk")

head(ccle_norm)
```