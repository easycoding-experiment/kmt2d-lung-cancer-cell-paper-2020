Super-enhancers defined by H3K4me1 and H3K27ac were identified by Samir using `ROSE`.

```bash
cd /rsrch1/genomic_med/krai/PendingSort/UCI_DATA/analyses/
find . -name *SuperEnhancers.bed  | parallel rsync -avhP {} /scratch/genomic_med/mtang1/scratch/UCI-hunain/superEnhancers/

cd !$
```

copy files to local computer:

```bash
cd /Users/mtang1/playground/hunai/bigwigs
rsync -avhP /Volumes/mdarisngc03.mdanderson.edu/scratch/UCI-hunain/bigwigs/*H3K27Ac*bw .

cd /Users/mtang1/playground/hunai/superEnhancers
rsync -avhP /Volumes/mdarisngc03.mdanderson.edu/scratch/UCI-hunain/superEnhancers/* .

## compare superEnhancers.
cat  Sample_{5,6}* | sort -k1,1 -k2,2n | bedtools merge -i - > WT_merged_superEnhancer.bed
cat *Mll4* | sort -k1,1 -k2,2n | bedtools merge -i - > Mll4_merged_superEnhancer.bed


```

```r
### H3K27ac super-enhancers
library(rtracklayer)
H3K27ac_bed<- import("superEnhancers/H3K27ac/WT_merged_superEnhancer.bed", format = "BED")
H3K27ac.50kb<- resize(H3K27ac_bed, width = 100000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.50kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.50kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = H3K27ac.50kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.50kb)

library(EnrichedHeatmap)
H3K27ac.center<- resize(H3K27ac.50kb, width =1, fix = "center")

RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=1000, extend = 50000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=1000, extend = 50000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=1000, extend = 50000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=1000, extend = 50000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-50kb", "center", "50kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-50kb", "center", "50kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-50kb", "center", "50kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-50kb", "center", "50kb")) 

```

For regular H3K27ac peaks, regular enhancers.

```bash
cd /Users/mtang1/playground/hunai/peaks/H3K27ac

cat RAS_1475-H3K27Ac_peaks.bed RAS_1508-1-H3K27Ac_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - > WT_H3K27ac_peaks.bed

cat MLL_1576-H3K27Ac_peaks.bed RAS_MLL-1527-H3K27Ac_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - > MLL_H3K27ac_peaks.bed

cat MLL_H3K27ac_peaks.bed WT_H3K27ac_peaks.bed| sort -k1,1 -k2,2n | bedtools merge -i - > total_peaks.bed

## one to one mapping
bedtools intersect -a total_peaks.bed -b MLL_H3K27ac_peaks.bed -wa | sort | uniq > MLL_peaks.bed
bedtools intersect -a total_peaks.bed -b WT_H3K27ac_peaks.bed -wa | sort | uniq > WT_peaks.bed

bedtools intersect -a WT_peaks.bed -b MLL_peaks.bed -v | sort | uniq > WT_unique_MLL_loss_H3K27ac_peaks.bed
15663
bedtools intersect -a WT_peaks.bed -b MLL_peaks.bed -wa | sort | uniq > WT_MLL_common_H3K27ac_peaks.bed
17697
bedtools intersect -a MLL_peaks.bed -b WT_peaks.bed -v | sort | uniq > MLL_unique_WT_loss_H3K27ac_peaks.bed
9903
```

```r

### H3K27ac all enhancers.
library(rtracklayer)
H3K27ac_bed<- import("/Users/mtang1/playground/hunai/peaks/H3K27ac/total_peaks.bed", format = "BED")
H3K27ac.10kb<- resize(H3K27ac_bed, width = 10000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = H3K27ac.10kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)

library(EnrichedHeatmap)
H3K27ac.center<- resize(H3K27ac.10kb, width =1, fix = "center")

RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 50), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))
#https://github.com/jokergoo/ComplexHeatmap/issues/57

set.seed(123)
mat<- cbind(RAS_1475.mat2, RAS_1508.mat2, MLL_1576.mat2, MLL_1527.mat2)
km<- kmeans(mat, 3)$cluster

EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

```

