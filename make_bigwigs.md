Moving data 

```bash
mtang1@cnode338:/rsrch1/genomic_med/krai/PendingSort/UCI_DATA/analyses/uci_railab_mm9$ 
find . -name "*sorted.bam" | parallel -j 6  rsync -avhP {} /scratch/genomic_med/mtang1/scratch/UCI-hunain/


```


### rename the files

```bash
#! /bin/bash

set -e
set -u
set -o pipefail

cat rename_antibody.txt | while read name sample antibody
do 
bam=${name/txt.gz/sorted.bam}
echo mv $bam $sample-$antibody.sorted.bam

done
```

make bigwigs

```bash
find *bam | parallel 'printf "samtools index {}\nbamCoverage -b {} --normalizeUsingRPKM --binSize 30 --smoothLength 300 -p 10 --extendReads 200 -o {/.}.bw" > {/.}_makebigwig.commands'

find . -name "*commands" | parallel 'makemsub -a {} -m 30g -o a -c 12 -j {/.} > {/.}.pbs'
```

### call peaks

`make_commands.sh`:  

```bash
#! /bin/bash
set -e
set -u
set -o pipefail

cat meta.txt | while read -r IP Input 
do 

prefix=$(basename $IP .sorted.bam)

JobString="

macs14 -t ${IP} -c ${Input} -n ${prefix} -p 1e-8 -g mm

"

echo "${JobString}" > call_peaks_${prefix}.txt
done
```

`./make_commands.sh`  
`find . -name "call_peaks*txt" | parallel 'makemsub -a {} -j {/.} -c 4 -t "02:00:00" -m 10g -o a > {/.}.msub`

```bash

for pbs in *msub
do
msub $pbs
sleep 1
done

```
