library(rtracklayer)
setwd("/Users/mtang1/playground/hunai")
H3K4me3_bed1<- import("peaks/RAS_1475_not_MLL_1576_H3K4me3_peaks.bed", format = "BED")
H3K4me3_bed2<- import("peaks/RAS_1475_and_MLL_1576_H3K4me3.bed", format = "BED")


H3K4me3.10kb<- resize(c(H3K4me3_bed1, H3K4me3_bed2), width = 20000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me3<- import("bigwigs/RAS_1475-H3K4me3.sorted.bw", format = "bigWig", which = H3K4me3.10kb)
RAS_1508.H3K4me3<- import("bigwigs/RAS_1508-1-H3K4me3.sorted.bw", format = "bigWig", which = H3K4me3.10kb)
MLL_1576.H3K4me3<- import("bigwigs/MLL_1576-H3K4me3.sorted.bw", format= "bigWig", which = H3K4me3.10kb)

library(EnrichedHeatmap)
H3K4me3.center<- resize(H3K4me3.10kb, width =1, fix = "center")

RAS_1475.mat<- normalizeToMatrix(RAS_1475.H3K4me3, H3K4me3.center, value_column = "score",
                           mean_mode="w0", w=100, extend = 10000)
RAS_1508.mat<- normalizeToMatrix(RAS_1508.H3K4me3, H3K4me3.center, value_column = "score",
                           mean_mode="w0", w=100, extend = 10000)
MLL_1576.mat<-normalizeToMatrix(MLL_1576.H3K4me3, H3K4me3.center, value_column = "score",
                            mean_mode="w0", w=100, extend = 10000)

set.seed(123)
EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 0, name = "RAS_1475 H3K4me3",
                column_title = "RAS_1475", use_raster = TRUE, row_order = 1:691) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 0, name = "RAS_1508 H3K4me3", 
                        column_title = "RAS_1508", use_raster = TRUE) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 0, name = "MLL_1576 H3K4me3", 
                        column_title ="MLL_1576", use_raster = TRUE) 

### mapping colors

library(circlize)
quantile(RAS_1475.mat, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat, probs = c(0.005, 0.5,0.995))

col_fun<- circlize::colorRamp2(c(0, 150), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 0, name = "RAS_1475 H3K4me3",
                column_title = "RAS_1475", use_raster = TRUE, 
                row_order = 1:8962, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 0, name = "RAS_1508 H3K4me3", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 0, name = "MLL_1576 H3K4me3", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) 

EnrichedHeatmap(mets.mat, axis_name_rot = 0, name = "mets H3K4me3",
                column_title = "mets", use_raster = TRUE, 
                row_order = 1:691, col= col_fun, split = c(rep("group1",250), rep("group2",441))) +
        EnrichedHeatmap(T4.mat, axis_name_rot = 0, name = "T4 H3K4me3", 
                        column_title ="T4", use_raster = TRUE, col= col_fun) +
        EnrichedHeatmap(T1.mat, axis_name_rot = 0, name = "T1 H3K4me3", 
                        column_title = "T1", use_raster = TRUE, col= col_fun) 



Heatmap(as.matrix(mets.met),  name = "mets H3K4me3", 
        column_title = "mets", use_raster = TRUE )

attributes(T1.mat)

########### H3K4me1

H3K4me1_bed<- import("peaks/H3K4me1_10_fold_peaks.bed", format = "BED")
H3K4me1.10kb<- resize(H3K4me1_bed, width = 10000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.10kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)

library(EnrichedHeatmap)
H3K4me1.center<- resize(H3K4me1.10kb, width =1, fix = "center")

RAS_1475.mat1<- normalizeToMatrix(RAS_1475.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat1<- normalizeToMatrix(RAS_1508.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat1<-normalizeToMatrix(MLL_1576.H3K4me1, H3K4me1.center, value_column = "score",
                                mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat1<-normalizeToMatrix(MLL_1527.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat1, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat1, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat1, axis_name_rot = 0, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, 
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat1, axis_name_rot = 0, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat1, axis_name_rot = 0, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat1, axis_name_rot = 0, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) 
        
RAS_1475_mean<- as.data.frame(colMeans(RAS_1475.mat1))
RAS_1508_mean<- as.data.frame(colMeans(RAS_1508.mat1))
MLL_1576_mean<- as.data.frame(colMeans(MLL_1576.mat1))
MLL_1527_mean<- as.data.frame(colMeans(MLL_1527.mat1))

H3K4me1_all<- cbind(RAS_1475_mean, RAS_1508_mean, MLL_1576_mean, MLL_1527_mean) %>%
        mutate(pos = rownames(RAS_1475_mean)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat1)`, RAS_1508 = `colMeans(RAS_1508.mat1)`,
                      MLL_1576 = `colMeans(MLL_1576.mat1)`, MLL_1527 = `colMeans(MLL_1527.mat1)`) %>%
        gather(sample, value, 1:4) 

H3K4me1_all$pos<- factor(H3K4me1_all$pos, levels= rownames(RAS_1475_mean))

ggplot(H3K4me1_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-50kb", "center", "50kb")) +
        xlab(NULL) + 
        ggtitle("H3K4me1 Super-enhancer signal")


H3K4me1_average<- H3K4me1_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

ggplot(H3K4me1_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-50kb", "center", "50kb")) +
        xlab(NULL) + 
        ggtitle("H3K4me1 Super-enhancer signal")

### H3K27ac super-enhancers
library(rtracklayer)
H3K27ac_bed<- import("superEnhancers/H3K27ac/WT_merged_superEnhancer.bed", format = "BED")
H3K27ac.2kb<- H3K27ac_bed + 2000

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.2kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.2kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = H3K27ac.2kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.2kb)

library(EnrichedHeatmap)

RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, H3K27ac_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, H3K27ac_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, H3K27ac_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, H3K27ac_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 45, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 12), 
                axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 45, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 12), 
                        axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 45, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 12),
                        axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 45, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 12),
                        axis_name = c("-2kb", "start", "end", "2kb"))


RAS_1475_mean2<- as.data.frame(colMeans(RAS_1475.mat2))
RAS_1508_mean2<- as.data.frame(colMeans(RAS_1508.mat2))
MLL_1576_mean2<- as.data.frame(colMeans(MLL_1576.mat2))
MLL_1527_mean2<- as.data.frame(colMeans(MLL_1527.mat2))

H3K27ac_all<- cbind(RAS_1475_mean2, RAS_1508_mean2, MLL_1576_mean2, MLL_1527_mean2) %>%
        mutate(pos = rownames(RAS_1475_mean2)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat2)`, RAS_1508 = `colMeans(RAS_1508.mat2)`,
                      MLL_1576 = `colMeans(MLL_1576.mat2)`, MLL_1527 = `colMeans(MLL_1527.mat2)`) %>%
        gather(sample, value, 1:4) 

H3K27ac_all$pos<- factor(H3K27ac_all$pos, levels= rownames(RAS_1475_mean2))

ggplot(H3K27ac_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "t1","t60","d20"), labels =c ("-2kb", "start", "end", "2kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac Super-enhancer signal")


H3K27ac_average<- H3K27ac_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

ggplot(H3K27ac_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "t1","t60","d20"), labels =c ("-2kb", "start", "end", "2kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac Super-enhancer signal")

############### H3K27ac all enhancers.
library(rtracklayer)
H3K27ac_bed<- import("/Users/mtang1/playground/hunai/peaks/H3K27ac/total_peaks.bed", format = "BED")
H3K27ac.10kb<- resize(H3K27ac_bed, width = 10000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = H3K27ac.10kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)

library(EnrichedHeatmap)
H3K27ac.center<- resize(H3K27ac.10kb, width =1, fix = "center")

RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))
#https://github.com/jokergoo/ComplexHeatmap/issues/57

set.seed(123)
mat<- cbind(RAS_1475.mat2, RAS_1508.mat2, MLL_1576.mat2, MLL_1527.mat2)
km<- kmeans(mat, 3)$cluster

EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

RAS_1475_mean2<- as.data.frame(colMeans(RAS_1475.mat2))
RAS_1508_mean2<- as.data.frame(colMeans(RAS_1508.mat2))
MLL_1576_mean2<- as.data.frame(colMeans(MLL_1576.mat2))
MLL_1527_mean2<- as.data.frame(colMeans(MLL_1527.mat2))

H3K27ac_all<- cbind(RAS_1475_mean2, RAS_1508_mean2, MLL_1576_mean2, MLL_1527_mean2) %>%
        mutate(pos = rownames(RAS_1475_mean2)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat2)`, RAS_1508 = `colMeans(RAS_1508.mat2)`,
                      MLL_1576 = `colMeans(MLL_1576.mat2)`, MLL_1527 = `colMeans(MLL_1527.mat2)`) %>%
        gather(sample, value, 1:4) 

H3K27ac_all$pos<- factor(H3K27ac_all$pos, levels= rownames(RAS_1475_mean2))

ggplot(H3K27ac_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1","d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac enhancer signal")


H3K27ac_average<- H3K27ac_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

ggplot(H3K27ac_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1","d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac enhancer signal")

###### H3K4me1 super-enhancer
setwd("~/playground/hunai/")
library(rtracklayer)
library(ComplexHeatmap)
H3K4me1_bed<- import("~/playground/hunai/superEnhancers/H3K4me1/WT_merged_H3K4me1_superEnhancer.bed", format = "BED")
H3K4me1.2kb<- H3K4me1_bed + 2000

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.2kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.2kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.2kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.2kb)

library(EnrichedHeatmap)

RAS_1475.mat1<- normalizeToMatrix(RAS_1475.H3K4me1, H3K4me1_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
RAS_1508.mat1<- normalizeToMatrix(RAS_1508.H3K4me1, H3K4me1_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
MLL_1576.mat1<-normalizeToMatrix(MLL_1576.H3K4me1, H3K4me1_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

MLL_1527.mat1<-normalizeToMatrix(MLL_1527.H3K4me1, H3K4me1_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat1, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat1, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 30), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat1, axis_name_rot = 45, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, 
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(RAS_1508.mat1, axis_name_rot = 45, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(MLL_1576.mat1, axis_name_rot = 45, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-2kb", "start", "end", "2kb")) +
        EnrichedHeatmap(MLL_1527.mat1, axis_name_rot = 45, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-2kb", "start", "end", "2kb")) 

RAS_1475_mean<- as.data.frame(colMeans(RAS_1475.mat1))
RAS_1508_mean<- as.data.frame(colMeans(RAS_1508.mat1))
MLL_1576_mean<- as.data.frame(colMeans(MLL_1576.mat1))
MLL_1527_mean<- as.data.frame(colMeans(MLL_1527.mat1))

H3K4me1_all<- cbind(RAS_1475_mean, RAS_1508_mean, MLL_1576_mean, MLL_1527_mean) %>%
        mutate(pos = rownames(RAS_1475_mean)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat1)`, RAS_1508 = `colMeans(RAS_1508.mat1)`,
                      MLL_1576 = `colMeans(MLL_1576.mat1)`, MLL_1527 = `colMeans(MLL_1527.mat1)`) %>%
        gather(sample, value, 1:4) 

H3K4me1_all$pos<- factor(H3K4me1_all$pos, levels= rownames(RAS_1475_mean))

ggplot(H3K4me1_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "t1","t60","d20"), labels =c ("-2kb", "start", "end", "2kb")) +
        xlab(NULL) + 
        ggtitle("H3K4me1 Super-enhancer signal")


H3K4me1_average<- H3K4me1_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

ggplot(H3K4me1_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "t1","t60","d20"), labels =c ("-2kb", "start", "end", "2kb")) +
        xlab(NULL) + 
        ggtitle("H3K4me1 Super-enhancer signal")
