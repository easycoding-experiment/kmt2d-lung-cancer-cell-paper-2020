---
title: "UCI_hunai_Enhancer_superEnhancer"
author: "Ming Tang"
date: "January 18, 2017"
output: html_document
---

### line plot together

```{r}
library(rtracklayer)
library(ComplexHeatmap)

setwd("/Users/mtang1/playground/hunai")
H3K4me1_bed<- import("peaks/H3K4me1_10_fold_peaks.bed", format = "BED")

H3K4me1_super_bed<- import("~/playground/hunai/superEnhancers/H3K4me1/WT_merged_H3K4me1_superEnhancer.bed", format = "BED")

H3K4me1_all_bed<- reduce(c(H3K4me1_bed, H3K4me1_super_bed))
H3K4me1_minus_super<- subsetByOverlaps(H3K4me1_bed, H3K4me1_super_bed, invert = TRUE, ignore.strand =T)

H3K4me1.10kb<- resize(H3K4me1_minus_super, width = 10000, fix = "center")

H3K4me1.10kb<- resize(H3K4me1_bed, width = 10000, fix = "center")

RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.10kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)

library(EnrichedHeatmap)
H3K4me1.center<- resize(H3K4me1.10kb, width =1, fix = "center")

RAS_1475.mat1<- normalizeToMatrix(RAS_1475.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat1<- normalizeToMatrix(RAS_1508.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat1<-normalizeToMatrix(MLL_1576.H3K4me1, H3K4me1.center, value_column = "score",
                                mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat1<-normalizeToMatrix(MLL_1527.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat1, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat1, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat1, axis_name_rot = 0, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, 
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat1, axis_name_rot = 0, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat1, axis_name_rot = 0, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat1, axis_name_rot = 0, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) 
        
RAS_1475_mean<- as.data.frame(colMeans(RAS_1475.mat1))
RAS_1508_mean<- as.data.frame(colMeans(RAS_1508.mat1))
MLL_1576_mean<- as.data.frame(colMeans(MLL_1576.mat1))
MLL_1527_mean<- as.data.frame(colMeans(MLL_1527.mat1))

H3K4me1_all<- cbind(RAS_1475_mean, RAS_1508_mean, MLL_1576_mean, MLL_1527_mean) %>%
        mutate(pos = rownames(RAS_1475_mean)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat1)`, RAS_1508 = `colMeans(RAS_1508.mat1)`,
                      MLL_1576 = `colMeans(MLL_1576.mat1)`, MLL_1527 = `colMeans(MLL_1527.mat1)`) %>%
        gather(sample, value, 1:4) 

H3K4me1_all$pos<- factor(H3K4me1_all$pos, levels= rownames(RAS_1475_mean))
H3K4me1_all$category<- "H3K4me1_minus_super"

####
H3K4me1_all_regular<- cbind(RAS_1475_mean, RAS_1508_mean, MLL_1576_mean, MLL_1527_mean) %>%
        mutate(pos = rownames(RAS_1475_mean)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat1)`, RAS_1508 = `colMeans(RAS_1508.mat1)`,
                      MLL_1576 = `colMeans(MLL_1576.mat1)`, MLL_1527 = `colMeans(MLL_1527.mat1)`) %>%
        gather(sample, value, 1:4) 

H3K4me1_all_regular$pos<- factor(H3K4me1_all_regular$pos, levels= rownames(RAS_1475_mean))
H3K4me1_all_regular$category<- "H3K4me1_enhancer"

bind_rows(H3K4me1_all, H3K4me1_all_regular)

ggplot(bind_rows(H3K4me1_all, H3K4me1_all_regular), aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        facet_wrap(~category) +
        ggtitle("H3K4me1 signal") + 
        theme(panel.spacing = unit(2, "lines"))


H3K4me1_average<- H3K4me1_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

H3K4me1_average$category<- "H3K4me1_minus_super"


H3K4me1_average_regular<- H3K4me1_all_regular %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

H3K4me1_average_regular$category<- "H3K4me1_enhancer"

ggplot(bind_rows(H3K4me1_average, H3K4me1_average_regular), aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        facet_wrap(~category) +
        ggtitle("H3K4me1 signal") +
        theme(panel.spacing = unit(2, "lines"))
```

### Make a function

```{r}

```

### boxplot

```{r}

H3K4me1_bed<- import("peaks/H3K4me1_10_fold_peaks.bed", format = "BED")

H3K4me1_super_bed<- import("~/playground/hunai/superEnhancers/H3K4me1/WT_merged_H3K4me1_superEnhancer.bed", format = "BED")

H3K4me1_all<- reduce(c(H3K4me1_bed, H3K4me1_super_bed))
H3K4me1_minus_super<- subsetByOverlaps(H3K4me1_bed, H3K4me1_super_bed, invert = TRUE, ignore.strand =T)

RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1_all_bed)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1_all_bed)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1_all_bed)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1_all_bed)

RAS_1475_binding_score<- coverage(RAS_1475.H3K4me1, weight = "score")
RAS_1508_binding_score<- coverage(RAS_1508.H3K4me1, weight = "score")
MLL_1576_binding_score<- coverage(MLL_1576.H3K4me1, weight = "score")
MLL_1527_binding_score<- coverage(MLL_1527.H3K4me1, weight = "score")

binnedSum <- function(bins, numvar, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewSums(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

## reorder the RleList to match the mm9_promoter seqlevels
RAS_1475_binding_score<- RAS_1475_binding_score[seqlevels(H3K4me1_bed)]
RAS_1508_binding_score<- RAS_1508_binding_score[seqlevels(H3K4me1_bed)]
MLL_1576_binding_score<- MLL_1576_binding_score[seqlevels(H3K4me1_bed)]
MLL_1527_binding_score<- MLL_1527_binding_score[seqlevels(H3K4me1_bed)]

H3K4me1_enhancer_all<- binnedSum(H3K4me1_bed, RAS_1475_binding_score, "RAS_1475")
H3K4me1_enhancer_all<- binnedSum(H3K4me1_enhancer_all, RAS_1508_binding_score, "RAS_1508")
H3K4me1_enhancer_all<- binnedSum(H3K4me1_enhancer_all, MLL_1576_binding_score, "MLL_1576")
H3K4me1_enhancer_all<- binnedSum(H3K4me1_enhancer_all, MLL_1527_binding_score, "MLL_1527")


H3K4me1_enhancer_all_binding<- as.data.frame(H3K4me1_enhancer_all) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))
H3K4me1_enhancer_all_binding$sample<- factor(H3K4me1_enhancer_all_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))

H3K4me1_enhancer_all_binding$category<- "all_enhancer"

ggplot(H3K4me1_enhancer_all_binding, aes(x =sample, y = log2(value))) + 
               geom_boxplot(aes(fill = treatment)) +
        theme_classic() +
        ggtitle("H3K4me1 signal for all enhancers") +
        ylab("log2 signal")

t.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

wilcox.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

### super-enhancer

H3K4me1_enhancer_super<- binnedSum(H3K4me1_super_bed, RAS_1475_binding_score, "RAS_1475")
H3K4me1_enhancer_super<- binnedSum(H3K4me1_enhancer_super, RAS_1508_binding_score, "RAS_1508")
H3K4me1_enhancer_super<- binnedSum(H3K4me1_enhancer_super, MLL_1576_binding_score, "MLL_1576")
H3K4me1_enhancer_super<- binnedSum(H3K4me1_enhancer_super, MLL_1527_binding_score, "MLL_1527")

H3K4me1_enhancer_super_binding<- as.data.frame(H3K4me1_enhancer_super) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))

H3K4me1_enhancer_super_binding$sample<- factor(H3K4me1_enhancer_super_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))
H3K4me1_enhancer_super_binding$category<- "super_enhancer"

ggplot(H3K4me1_enhancer_super_binding, aes(x =sample, y = log2(value))) + 
               geom_boxplot(aes(fill = treatment)) +
        theme_classic() +
        ggtitle("H3K4me1 signal for Super enhancers") +
        ylab("log2 signal")

### enhancer minus superenhancer

H3K4me1_enhancer_minus_super<- binnedSum(H3K4me1_minus_super, RAS_1475_binding_score, "RAS_1475")
H3K4me1_enhancer_minus_super<- binnedSum(H3K4me1_enhancer_minus_super, RAS_1508_binding_score, "RAS_1508")
H3K4me1_enhancer_minus_super<- binnedSum(H3K4me1_enhancer_minus_super, MLL_1576_binding_score, "MLL_1576")
H3K4me1_enhancer_minus_super<- binnedSum(H3K4me1_enhancer_minus_super, MLL_1527_binding_score, "MLL_1527")

H3K4me1_enhancer_minus_super_binding<- as.data.frame(H3K4me1_enhancer_minus_super) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))

H3K4me1_enhancer_minus_super_binding$sample<- factor(H3K4me1_enhancer_minus_super_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))
H3K4me1_enhancer_minus_super_binding$category<- "enhancer_minus_super"

wilcox.test(as.data.frame(H3K4me1_enhancer_minus_super)$RAS_1508, as.data.frame(H3K4me1_enhancer_minus_super)$MLL_1576, paired = T)

t.test(as.data.frame(H3K4me1_enhancer_minus_super)$RAS_1508, as.data.frame(H3K4me1_enhancer_minus_super)$MLL_1576, paired = T)


ggplot(bind_rows(H3K4me1_enhancer_all_binding,H3K4me1_enhancer_super_binding, H3K4me1_enhancer_minus_super_binding), aes(x = sample, y = log2(value))) + 
        geom_boxplot(aes(fill = treatment)) + 
        facet_wrap(~category) +
        ggtitle("H3K4me1 signal for enhancers") +
        ylab("log2 signal") + theme(axis.text.x = element_text(hjust =1, angle = 45))
```



### H3K27ac line graph


```{r}
############### H3K27ac all enhancers.
library(rtracklayer)
H3K27ac_bed<- import("/Users/mtang1/playground/hunai/peaks/H3K27ac/total_peaks.bed", format = "BED")
H3K27ac_super_bed<- import("superEnhancers/H3K27ac/WT_merged_superEnhancer.bed", format = "BED")
H3K27ac_all_bed<- reduce(c(H3K27ac_bed, H3K27ac_super_bed))

H3K27ac_minus_super<- subsetByOverlaps(H3K27ac_bed, H3K27ac_super_bed, invert = TRUE, ignore.strand =T)

H3K27ac.10kb<- resize(H3K27ac_bed, width = 10000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = H3K27ac.10kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = H3K27ac.10kb)

library(EnrichedHeatmap)
H3K27ac.center<- resize(H3K27ac.10kb, width =1, fix = "center")

RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))
#https://github.com/jokergoo/ComplexHeatmap/issues/57

set.seed(123)
mat<- cbind(RAS_1475.mat2, RAS_1508.mat2, MLL_1576.mat2, MLL_1527.mat2)
km<- kmeans(mat, 3)$cluster

EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

RAS_1475_mean2<- as.data.frame(colMeans(RAS_1475.mat2))
RAS_1508_mean2<- as.data.frame(colMeans(RAS_1508.mat2))
MLL_1576_mean2<- as.data.frame(colMeans(MLL_1576.mat2))
MLL_1527_mean2<- as.data.frame(colMeans(MLL_1527.mat2))

H3K27ac_all<- cbind(RAS_1475_mean2, RAS_1508_mean2, MLL_1576_mean2, MLL_1527_mean2) %>%
        mutate(pos = rownames(RAS_1475_mean2)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat2)`, RAS_1508 = `colMeans(RAS_1508.mat2)`,
                      MLL_1576 = `colMeans(MLL_1576.mat2)`, MLL_1527 = `colMeans(MLL_1527.mat2)`) %>%
        gather(sample, value, 1:4) 

H3K27ac_all_regular<- cbind(RAS_1475_mean2, RAS_1508_mean2, MLL_1576_mean2, MLL_1527_mean2) %>%
        mutate(pos = rownames(RAS_1475_mean2)) %>%
        dplyr::rename(RAS_1457 = `colMeans(RAS_1475.mat2)`, RAS_1508 = `colMeans(RAS_1508.mat2)`,
                      MLL_1576 = `colMeans(MLL_1576.mat2)`, MLL_1527 = `colMeans(MLL_1527.mat2)`) %>%
        gather(sample, value, 1:4) 

H3K27ac_all$pos<- factor(H3K27ac_all$pos, levels= rownames(RAS_1475_mean2))
H3K27ac_all$category<- "H3K27ac_minus_super"

H3K27ac_all_regular$pos<- factor(H3K27ac_all$pos, levels= rownames(RAS_1475_mean2))
H3K27ac_all_regular$category<- "H3K27ac_enhancer"

ggplot(H3K27ac_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1","d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac enhancer signal")


H3K27ac_average<- H3K27ac_all %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))
H3K27ac_average$category<- "H3K27ac_minus_super"

H3K27ac_average_regular<- H3K27ac_all_regular %>% separate(sample, c("sample", "replicate")) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))
H3K27ac_average_regular$category<- "H3K27ac_enhancer"

ggplot(H3K27ac_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1","d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27ac enhancer signal")

ggplot(bind_rows(H3K27ac_all, H3K27ac_all_regular), aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        facet_wrap(~category) +
        ggtitle("H3K27ac signal") + 
        theme(panel.spacing = unit(2, "lines"))

ggplot(bind_rows(H3K27ac_average, H3K27ac_average_regular), aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample)) + 
        theme_classic(base_size = 14) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        facet_wrap(~category) +
        ggtitle("H3K27ac signal") +
        theme(panel.spacing = unit(2, "lines"))

```

### boxplot for H3K27ac

```{r}

H3K27ac_bed<- import("/Users/mtang1/playground/hunai/peaks/H3K27ac/total_peaks.bed", format = "BED")
H3K27ac_super_bed<- import("superEnhancers/H3K27ac/WT_merged_superEnhancer.bed", format = "BED")
H3K27ac_all_bed<- reduce(c(H3K27ac_bed, H3K27ac_super_bed))

H3K27ac_minus_super<- subsetByOverlaps(H3K27ac_bed, H3K27ac_super_bed, invert = TRUE, ignore.strand =T)


RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27ac.sorted.bw", format = "bigWig", which = H3K27ac_all_bed)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27ac.sorted.bw", format = "bigWig", which = H3K27ac_all_bed)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27ac.sorted.bw", format= "bigWig", which = H3K27ac_all_bed)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27ac.sorted.bw", format = "bigWig", which = H3K27ac_all_bed)

RAS_1475_binding_score<- coverage(RAS_1475.H3K27ac, weight = "score")
RAS_1508_binding_score<- coverage(RAS_1508.H3K27ac, weight = "score")
MLL_1576_binding_score<- coverage(MLL_1576.H3K27ac, weight = "score")
MLL_1527_binding_score<- coverage(MLL_1527.H3K27ac, weight = "score")

binnedSum <- function(bins, numvar, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewSums(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

## reorder the RleList to match the mm9_promoter seqlevels
RAS_1475_binding_score<- RAS_1475_binding_score[seqlevels(H3K27ac_bed)]
RAS_1508_binding_score<- RAS_1508_binding_score[seqlevels(H3K27ac_bed)]
MLL_1576_binding_score<- MLL_1576_binding_score[seqlevels(H3K27ac_bed)]
MLL_1527_binding_score<- MLL_1527_binding_score[seqlevels(H3K27ac_bed)]

H3K27ac_enhancer_all<- binnedSum(H3K27ac_bed, RAS_1475_binding_score, "RAS_1475")
H3K27ac_enhancer_all<- binnedSum(H3K27ac_enhancer_all, RAS_1508_binding_score, "RAS_1508")
H3K27ac_enhancer_all<- binnedSum(H3K27ac_enhancer_all, MLL_1576_binding_score, "MLL_1576")
H3K27ac_enhancer_all<- binnedSum(H3K27ac_enhancer_all, MLL_1527_binding_score, "MLL_1527")


H3K27ac_enhancer_all_binding<- as.data.frame(H3K27ac_enhancer_all) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))
H3K27ac_enhancer_all_binding$sample<- factor(H3K27ac_enhancer_all_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))

H3K27ac_enhancer_all_binding$category<- "all_enhancer"



t.test(as.data.frame(H3K27ac_enhancer_all)$RAS_1508, as.data.frame(H3K27ac_enhancer_all)$MLL_1576, paired = T)

wilcox.test(as.data.frame(H3K27ac_enhancer_all)$RAS_1508, as.data.frame(H3K27ac_enhancer_all)$MLL_1576, paired = T)

### super-enhancer

RAS_1475_binding_score<- RAS_1475_binding_score[seqlevels(H3K27ac_super_bed)]
RAS_1508_binding_score<- RAS_1508_binding_score[seqlevels(H3K27ac_super_bed)]
MLL_1576_binding_score<- MLL_1576_binding_score[seqlevels(H3K27ac_super_bed)]
MLL_1527_binding_score<- MLL_1527_binding_score[seqlevels(H3K27ac_super_bed)]

H3K27ac_enhancer_super<- binnedSum(H3K27ac_super_bed, RAS_1475_binding_score, "RAS_1475")
H3K27ac_enhancer_super<- binnedSum(H3K27ac_enhancer_super, RAS_1508_binding_score, "RAS_1508")
H3K27ac_enhancer_super<- binnedSum(H3K27ac_enhancer_super, MLL_1576_binding_score, "MLL_1576")
H3K27ac_enhancer_super<- binnedSum(H3K27ac_enhancer_super, MLL_1527_binding_score, "MLL_1527")

H3K27ac_enhancer_super_binding<- as.data.frame(H3K27ac_enhancer_super) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))

H3K27ac_enhancer_super_binding$sample<- factor(H3K27ac_enhancer_super_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))
H3K27ac_enhancer_super_binding$category<- "super_enhancer"

### enhancer minus superenhancer
RAS_1475_binding_score<- RAS_1475_binding_score[seqlevels(H3K27ac_minus_super)]
RAS_1508_binding_score<- RAS_1508_binding_score[seqlevels(H3K27ac_minus_super)]
MLL_1576_binding_score<- MLL_1576_binding_score[seqlevels(H3K27ac_minus_super)]
MLL_1527_binding_score<- MLL_1527_binding_score[seqlevels(H3K27ac_minus_super)]

H3K27ac_enhancer_minus_super<- binnedSum(H3K27ac_minus_super, RAS_1475_binding_score, "RAS_1475")
H3K27ac_enhancer_minus_super<- binnedSum(H3K27ac_enhancer_minus_super, RAS_1508_binding_score, "RAS_1508")
H3K27ac_enhancer_minus_super<- binnedSum(H3K27ac_enhancer_minus_super, MLL_1576_binding_score, "MLL_1576")
H3K27ac_enhancer_minus_super<- binnedSum(H3K27ac_enhancer_minus_super, MLL_1527_binding_score, "MLL_1527")

H3K27ac_enhancer_minus_super_binding<- as.data.frame(H3K27ac_enhancer_minus_super) %>% 
        gather(sample, value, 6:9) %>% separate(sample, c("treatment", "replicate")) %>% 
        mutate(sample = paste(treatment, replicate, sep = "_"))

H3K27ac_enhancer_minus_super_binding$sample<- factor(H3K27ac_enhancer_minus_super_binding$sample, levels = c("RAS_1475", "RAS_1508", "MLL_1527", "MLL_1576"))
H3K27ac_enhancer_minus_super_binding$category<- "enhancer_minus_super"


ggplot(bind_rows(H3K27ac_enhancer_all_binding,H3K27ac_enhancer_super_binding, H3K27ac_enhancer_minus_super_binding), aes(x = sample, y = log2(value))) + 
        geom_boxplot(aes(fill = treatment)) + 
        facet_wrap(~category) +
        ggtitle("H3K27ac signal for enhancers") +
        ylab("log2 signal") + theme(axis.text.x = element_text(hjust =1, angle = 45)) +
        ylim(5,25)
```
