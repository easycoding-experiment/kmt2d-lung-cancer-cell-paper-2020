

```bash
cd /rsrch2/genomic_med/krai/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/11bed

wc -l *

24 cellmarkfiletable.txt
   20135116 MLL_1527_A.bed
   20040164 MLL_1527_B.bed
   18368037 MLL_1527_C.bed
   19523171 MLL_1527_D.bed
    2151444 MLL_1527_E.bed
   19858236 MLL_1527_F.bed
   18473402 MLL_1527_G.bed
   16934324 MLL_1576_A.bed
   20000313 MLL_1576_B.bed
   17750656 MLL_1576_C.bed
   18135469 MLL_1576_D.bed
   19982689 MLL_1576_E.bed
   19886164 MLL_1576_F.bed
   18807163 MLL_1576_G.bed
   16652599 RAS_1475_A.bed
    9782283 RAS_1475_B.bed
   20035054 RAS_1475_C.bed
   19047434 RAS_1475_D.bed
   17010395 RAS_1475_E.bed
   20302024 RAS_1475_F.bed
   19234237 RAS_1475_G.bed
   21033373 RAS_1508_A.bed
   20221130 RAS_1508_B.bed
   20245898 RAS_1508_C.bed
   20500105 RAS_1508_D.bed
   20047154 RAS_1508_E.bed
   20763559 RAS_1508_F.bed
   18826389 RAS_1508_G.bed

```

For now, downsample all E to 2151444 and all B to 9782283

```bash
mv MLL_1576_E.bed MLL_1576_E_old.bed 
mv RAS_1475_E.bed RAS_1475_E_old.bed
mv RAS_1508_E.bed RAS_1508_E_old.bed

cat MLL_1576_E_old.bed | shuf -n 2151444 > MLL_1576_E.bed

cat RAS_1475_E_old.bed | shuf -n 2151444 > RAS_1475_E.bed

cat RAS_1508_E_old.bed | shuf -n 2151444 > RAS_1508_E.bed

mv MLL_1527_B.bed MLL_1527_B_old.bed
mv MLL_1576_B.bed MLL_1576_B_old.bed
mv RAS_1508_B.bed RAS_1508_B_old.bed

cat MLL_1527_B_old.bed | shuf -n 9782283 > MLL_1527_B.bed
cat MLL_1576_B_old.bed | shuf -n 9782283 > MLL_1576_B.bed
cat RAS_1508_B_old.bed | shuf -n 9782283 > RAS_1508_B.bed

```

run chromHMM

```bash
./pyChIP-seq.sh --until chromHmm_learn
```